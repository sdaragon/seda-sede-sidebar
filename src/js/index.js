// Dependencies for dropdownComponent and listboxComponent:
// https://unpkg.com/@popperjs/core@2.4.4/dist/umd/popper.min.js
// https://unpkg.com/tippy.js@6.2.6/dist/tippy-bundle.umd.min.js


import {
  accordionComponent,
  alertComponent,
  collapsibleComponent,
  dialogComponent,
  dropdownComponent,
  listboxComponent,
  menubarComponent,
  tableAdvancedComponent,
  tabsComponent,
  tooltipComponent,
  toggleComponent,
  treeComponent,
  notificationComponent,
  radioButtonComponent,
  checkBoxComponent,
  MenuVerticalComponent,
  MenuHorizontalComponent,
  MenuNavigationComponent,
  HeaderNavigationComponent,
  NavComponent
} from './desy-html.js';

var aria = aria || {};

accordionComponent(aria);
alertComponent(aria);
collapsibleComponent(aria);
dialogComponent(aria);
dropdownComponent(aria);
listboxComponent(aria);
menubarComponent(aria);
tableAdvancedComponent(aria);
tabsComponent(aria);
tooltipComponent(aria);
toggleComponent(aria);
treeComponent(aria);
notificationComponent(aria);
radioButtonComponent(aria);
checkBoxComponent(aria);
MenuVerticalComponent(aria);
MenuHorizontalComponent(aria);
MenuNavigationComponent(aria);
HeaderNavigationComponent(aria);
NavComponent(aria);




const { createMachine, actions, interpret } = XState;

var show = function(el){
  var element = document.getElementById(el);
  element.classList.remove('hidden');
  element.classList.add('u-ring');
}

var hide = function(el){
  var element = document.getElementById(el);
  element.classList.add('hidden');
  element.classList.remove('u-ring');
}

var addClass = function(el, customClass){
  var element = document.getElementById(el);
  element.classList.add(customClass);
}

var removeClass = function(el, customClass){
  var element = document.getElementById(el);
  element.classList.remove(customClass);
}

var changeClass = function(el, customClass){
  var element = document.getElementById(el);
  element.className = '';
  element.classList.add(customClass);
}

const lightMachine = createMachine(
  {
    id: "seda-sede-sidebar-0",
    initial: "formaElectronicoPersonalFuncionario",
    states: {
      formaElectronicoPersonalFuncionario: {
        initial: "formaElectronicoPersonalFuncionarioHabilitadoUnchecked",
        states: {
          formaElectronicoPersonalFuncionarioHabilitadoUnchecked: {
            entry: (context, event) => {
              console.log(
                "formaElectronicoPersonalFuncionarioHabilitadoUnchecked entrada",
              );
            },
            exit: (context, event) => {
              console.log("formaElectronicoPersonalFuncionarioHabilitadoUnchecked salida");
            },
            on: {
              FORMAELECTRONICOPERSONALFUNCIONARIOHABILITADOCHECKED: {
                target: "formaElectronicoPersonalFuncionarioHabilitadoChecked",
              },
            },
          },
          formaElectronicoPersonalFuncionarioHabilitadoChecked: {
            entry: (context, event) => {
              console.log("formaElectronicoPersonalFuncionarioHabilitadoChecked entrada");
            },
            exit: (context, event) => {
              console.log("formaElectronicoPersonalFuncionarioHabilitadoChecked salida");
            },
            on: {
              FORMAELECTRONICOPERSONALFUNCIONARIOHABILITADOUNCHECKED: {
                target: "formaElectronicoPersonalFuncionarioHabilitadoUnchecked",
              },
            },
          },
        },
      },
      plazos: {
        initial: "enPlazo",
        states: {
          enPlazo: {
            entry: (context, event) => {
              show("enPlazo");
              show("enPlazo-content");
            },
            exit: (context, event) => {
              hide("enPlazo");
              hide("enPlazo-content");
            },
          },
          fueraDePlazo: {
            entry: (context, event) => {
              show("fueraDePlazo");
              show("fueraDePlazo-content");
            },
            exit: (context, event) => {
              hide("fueraDePlazo");
              hide("fueraDePlazo-content");
            },
          },
          plazoProximo: {
            entry: (context, event) => {
              show("plazoProximo");
              show("plazoProximo-content");
            },
            exit: (context, event) => {
              hide("plazoProximo");
              hide("plazoProximo-content");
            },
          },
          plazoPendiente: {
            entry: (context, event) => {
              show("plazoPendiente");
              show("plazoPendiente-content");
            },
            exit: (context, event) => {
              hide("plazoPendiente");
              hide("plazoPendiente-content");
            },
          },
        },
        on: {
          PLAZOPENDIENTE: {
            target: ".plazoPendiente",
          },
          PLAZOPROXIMO: {
            target: ".plazoProximo",
          },
          FUERADEPLAZO: {
            target: ".fueraDePlazo",
          },
          ENPLAZO: {
            target: ".enPlazo",
          },
        },
      },
      formaPresentacion: {
        states: {
          formaPresencial: {
            initial: "formaPresencialUnchecked",
            states: {
              formaPresencialUnchecked: {
                on: {
                  FORMAPRESENCIALCHECKED: {
                    target: "formaPresencialChecked",
                  },
                },
              },
              formaPresencialChecked: {
                entry: (context, event) => {
                  show("formaPresencialChecked");
                  show("formaPresencialChecked-content");
                  show("oficinasDeRegistroChecked");
                  show("oficinasDeRegistroChecked-content");
                },
                exit: (context, event) => {
                  hide("formaPresencialChecked");
                  hide("formaPresencialChecked-content");
                },
                states: {
                  oficinasDeRegistro: {
                    initial: "oficinasDeRegistroUnchecked",
                    states: {
                      oficinasDeRegistroUnchecked: {
                        on: {
                          OFICINASDEREGISTROCHECKED: {
                            target: "oficinasDeRegistroChecked",
                          },
                        },
                      },
                      oficinasDeRegistroChecked: {
                        entry: (context, event) => {
                          show("oficinasDeRegistroChecked");
                          show("oficinasDeRegistroChecked-content");
                        },
                        on: {
                          OFICINASDEREGISTROUNCHECKED: {
                            target: "oficinasDeRegistroUnchecked",
                          },
                        },
                      },
                    },
                  },
                  otroSitio: {
                    initial: "otroSitioUnchecked",
                    states: {
                      otroSitioUnchecked: {
                        on: {
                          OTROSITIOCHECKED: {
                            target: "otroSitioChecked",
                          },
                        },
                      },
                      otroSitioChecked: {
                        entry: (context, event) => {
                          hide("oficinasDeRegistroChecked");
                          hide("oficinasDeRegistroChecked-content");
                          show("otroSitioChecked");
                          show("otroSitioChecked-content");
                        },
                        exit: (context, event) => {
                          show("oficinasDeRegistroChecked");
                          show("oficinasDeRegistroChecked-content");
                          hide("otroSitioChecked");
                          hide("otroSitioChecked-content");
                        },
                        on: {
                          OTROSITIOUNCHECKED: {
                            target: "otroSitioUnchecked",
                          },
                        },
                      },
                    },
                  },
                },
                on: {
                  FORMAPRESENCIALUNCHECKED: {
                    target: "formaPresencialUnchecked",
                  },
                },
                type: "parallel",
              },
            },
          },
          formaElectronico: {
            initial: "formaElectronicoUnchecked",
            states: {
              formaElectronicoUnchecked: {
                on: {
                  FORMAELECTRONICOCHECKED: {
                    target: "formaElectronicoChecked",
                  },
                },
              },
              formaElectronicoChecked: {
                entry: (context, event) => {
                  show("formaElectronicoChecked");
                  show("formaElectronicoChecked-content");
                },
                exit: (context, event) => {
                  hide("formaElectronicoChecked");
                  hide("formaElectronicoChecked-content");
                },
                initial: "formaElectronicoOpcionesAdicionales",
                states: {
                  formaElectronicoOpcionesAdicionales: {
                    initial: "formaElectronicoOpcionesAdicionalesUnchecked",
                    states: {
                      formaElectronicoOpcionesAdicionalesUnchecked: {
                        entry: (context, event) => {
                          console.log("formaElectronicoOpcionesAdicionalesUnchecked entrada");
                        },
                        exit: (context, event) => {
                          console.log("formaElectronicoOpcionesAdicionalesUnchecked salida");
                        },
                        on: {
                          FORMAELECTRONICOOPCIONESADICIONALESCHECKED: {
                            target: "formaElectronicoOpcionesAdicionalesChecked",
                          },
                        },
                      },
                      formaElectronicoOpcionesAdicionalesChecked: {
                        entry: (context, event) => {
                          console.log("formaElectronicoOpcionesAdicionalesChecked entrada");
                        },
                        exit: (context, event) => {
                          console.log("formaElectronicoOpcionesAdicionalesChecked salida");
                        },
                        on: {
                          FORMAELECTRONICOOPCIONESADICIONALESUNCHECKED: {
                            target: "formaElectronicoOpcionesAdicionalesUnchecked",
                          },
                        },
                      },
                    },
                  },
                  opcionesDeSolicitudElectronica: {
                    initial: "tto",
                    states: {
                      tto: {
                        entry: (context, event) => {
                          show("tto");
                          show("tto-content");
                        },
                        exit: (context, event) => {
                          hide("tto");
                          hide("tto-content");
                        },
                      },
                      rega: {
                        entry: (context, event) => {
                          show("rega");
                          show("rega-content");
                        },
                        exit: (context, event) => {
                          hide("rega");
                          hide("rega-content");
                        },
                      },
                      propia: {
                        entry: (context, event) => {
                          show("propia");
                          show("propia-content");
                        },
                        exit: (context, event) => {
                          hide("propia");
                          hide("propia-content");
                        },
                      },
                    },
                    on: {
                      REGA: {
                        target: ".rega",
                      },
                      PROPIA: {
                        target: ".propia",
                      },
                      TTO: {
                        target: ".tto",
                      },
                    },
                  },
                },
                on: {
                  FORMAELECTRONICOUNCHECKED: {
                    target: "formaElectronicoUnchecked",
                  },
                },
                type: "parallel",
              },
            },
          },
        },
        type: "parallel",
      },
      modelo: {
        initial: "siNecesario",
        states: {
          siNecesario: {
            entry: (context, event) => {
              show("modelo-oficinasDeRegistro-siNecesario");
              show("modelo-oficinasDeRegistro-siNecesario-content");
              show("modelo-otroSitio-siNecesario");
              show("modelo-otroSitio-siNecesario-content");
              show("rega-siNecesario");
              show("rega-siNecesario-content");
            },
            exit: (context, event) => {
              hide("modelo-oficinasDeRegistro-siNecesario");
              hide("modelo-oficinasDeRegistro-siNecesario-content");
              hide("modelo-otroSitio-siNecesario");
              hide("modelo-otroSitio-siNecesario-content");
              hide("rega-siNecesario");
              hide("rega-siNecesario-content");
            },
            on: {
              NONECESARIO: {
                target: "noNecesario",
              },
            },
          },
          noNecesario: {
            entry: (context, event) => {
              show("modelo-oficinasDeRegistro-noNecesario");
              show("modelo-oficinasDeRegistro-noNecesario-content");
              show("modelo-otroSitio-noNecesario");
              show("modelo-otroSitio-noNecesario-content");
              show("rega-noNecesario");
              show("rega-noNecesario-content");
            },
            exit: (context, event) => {
              hide("modelo-oficinasDeRegistro-noNecesario");
              hide("modelo-oficinasDeRegistro-noNecesario-content");
              hide("modelo-otroSitio-noNecesario");
              hide("modelo-otroSitio-noNecesario-content");
              hide("rega-noNecesario");
              hide("rega-noNecesario-content");
            },
            on: {
              SINECESARIO: {
                target: "siNecesario",
              },
            },
          },
        },
      },
      fechas: {
        initial: "fechasPermanente",
        states: {
          fechasPermanente: {
            entry: (context, event) => {
              show("fechas-enPlazo-fechasPermanente");
              show("fechas-plazoProximo-fechasPermanente");
            },
            exit: (context, event) => {
              hide("fechas-enPlazo-fechasPermanente");
              hide("fechas-plazoProximo-fechasPermanente");
            },
            on: {
              FECHASTEMPORAL: {
                target: "fechasTemporal",
              },
            },
          },
          fechasTemporal: {
            entry: (context, event) => {
              show("fechas-enPlazo-fechasTemporal");
              show("fechas-plazoProximo-fechasTemporal");
            },
            exit: (context, event) => {
              hide("fechas-enPlazo-fechasTemporal");
              hide("fechas-plazoProximo-fechasTemporal");
            },
            initial: "temporalInicioFin",
            states: {
              temporalInicioFin: {
                entry: (context, event) => {
                  show("fechas-enPlazo-fechasTemporal-temporalInicioFin");
                  show("fechas-plazoProximo-fechasTemporal-temporalInicioFin");
                },
                exit: (context, event) => {
                  hide("fechas-enPlazo-fechasTemporal-temporalInicioFin");
                  hide("fechas-plazoProximo-fechasTemporal-temporalInicioFin");
                },
              },
              temporalInicio: {
                entry: (context, event) => {
                  show("fechas-enPlazo-fechasTemporal-temporalInicio");
                  show("fechas-plazoProximo-fechasTemporal-temporalInicio");
                },
                exit: (context, event) => {
                  hide("fechas-enPlazo-fechasTemporal-temporalInicio");
                  hide("fechas-plazoProximo-fechasTemporal-temporalInicio");
                },
              },
              temporalFin: {
                entry: (context, event) => {
                  show("fechas-enPlazo-fechasTemporal-temporalFin");
                  show("fechas-plazoProximo-fechasTemporal-temporalFin");
                },
                exit: (context, event) => {
                  hide("fechas-enPlazo-fechasTemporal-temporalFin");
                  hide("fechas-plazoProximo-fechasTemporal-temporalFin");
                },
              },
              temporalVacios: {
                entry: (context, event) => {
                  show("fechas-enPlazo-fechasTemporal-temporalVacios");
                  show("fechas-plazoProximo-fechasTemporal-temporalVacios");
                },
                exit: (context, event) => {
                  hide("fechas-enPlazo-fechasTemporal-temporalVacios");
                  hide("fechas-plazoProximo-fechasTemporal-temporalVacios");
                },
              },
            },
            on: {
              TEMPORALINICIO: {
                target: ".temporalInicio",
              },
              TEMPORALFIN: {
                target: ".temporalFin",
              },
              TEMPORALVACIOS: {
                target: ".temporalVacios",
              },
              TEMPORALINICIOFIN: {
                target: ".temporalInicioFin",
              },
              FECHASPERMANENTE: {
                target: "fechasPermanente",
              },
            },
          },
        },
      },
      inicio: {
        states: {
          inicioDeOficio: {
            initial: "inicioDeOficioUnchecked",
            states: {
              inicioDeOficioUnchecked: {
                entry: (context, event) => {},
                exit: (context, event) => {},
                on: {
                  INICIODEOFICIOCHECKED: {
                    target: "inicioDeOficioChecked",
                  },
                },
              },
              inicioDeOficioChecked: {
                entry: (context, event) => {},
                exit: (context, event) => {},
                on: {
                  INICIODEOFICIOUNCHECKED: {
                    target: "inicioDeOficioUnchecked",
                  },
                },
              },
            },
          },
          inicioAInstancia: {
            initial: "inicioAInstanciaUnchecked",
            states: {
              inicioAInstanciaUnchecked: {
                entry: (context, event) => {},
                exit: (context, event) => {},
                on: {
                  INICIOAINSTANCIACHECKED: {
                    target: "inicioAInstanciaChecked",
                  },
                },
              },
              inicioAInstanciaChecked: {
                entry: (context, event) => {},
                exit: (context, event) => {},
                on: {
                  INICIOAINSTANCIAUNCHECKED: {
                    target: "inicioAInstanciaUnchecked",
                  },
                },
              },
            },
          },
        },
        type: "parallel",
      },
      otrasCondicionesEspecificasDePresentacionDeLaSolicitud: {
        initial: "otrasCondicionesEspecificasDePresentacionDeLaSolicitudUnchecked",
        states: {
          otrasCondicionesEspecificasDePresentacionDeLaSolicitudUnchecked: {
            entry: (context, event) => {
              console.log(
                "otrasCondicionesEspecificasDePresentacionDeLaSolicitudUnchecked entrada",
              );
            },
            exit: (context, event) => {
              console.log("otrasCondicionesEspecificasDePresentacionDeLaSolicitudUnchecked salida");
            },
            on: {
              OTRASCONDICIONESESPECIFICASDEPRESENTACIONDELASOLICITUDCHECKED: {
                target: "otrasCondicionesEspecificasDePresentacionDeLaSolicitudChecked",
              },
            },
          },
          otrasCondicionesEspecificasDePresentacionDeLaSolicitudChecked: {
            entry: (context, event) => {
              console.log("otrasCondicionesEspecificasDePresentacionDeLaSolicitudChecked entrada");
            },
            exit: (context, event) => {
              console.log("otrasCondicionesEspecificasDePresentacionDeLaSolicitudChecked salida");
            },
            on: {
              OTRASCONDICIONESESPECIFICASDEPRESENTACIONDELASOLICITUDUNCHECKED: {
                target: "otrasCondicionesEspecificasDePresentacionDeLaSolicitudUnchecked",
              },
            },
          },
        },
      },
    },
    type: "parallel",
    predictableActionArguments: true,
    preserveActionOrder: true,
  },
  {
    actions: {},
    services: {},
    guards: {},
    delays: {},
  },
);

const lightService = interpret(lightMachine);
lightService.onTransition(state => {
  if (state.changed) {
    // para debuguear los estados activados en cada cambio de estado
    console.log(state.toStrings());
    if (state.matches('formaPresentacion.formaElectronico.formaElectronicoChecked')){
      // si está  mostrado formaElectrónico, entonces lo mostramos solo
      hide('formaPresencial');
    } else {
      show('formaPresencial');
    }
    if (state.matches('inicio.inicioAInstancia.inicioAInstanciaChecked')){
      // si está mostrado inicioAInstancia, entonces mostramos subsanación
      show('subsanacion');
    } else {
      hide('subsanacion');
    }
    if (state.matches('inicio.inicioDeOficio.inicioDeOficioChecked')||state.matches('inicio.inicioAInstancia.inicioAInstanciaChecked')){
      // si está  mostrado inicioDeOficio y/o inicioAInstancia entonces mostramos aportación
      show('aportacion');
    } else {
      hide('aportacion');
    }
    if (state.matches('fechas.fechasTemporal.temporalVacios')){
      // si está Temporal con Plazos de inicio y fin vacíos es como si fuera Plazo pendiente de definir
      hide('formaPresencial');
    }

  }
});

window.inicioDeOficio = function() {
  const checkedValue = document.querySelector('input[name="inicio-de-oficio"]').checked;
  if (checkedValue == true) {
    lightService.send('INICIODEOFICIOCHECKED');
  } else {
    lightService.send('INICIODEOFICIOUNCHECKED');
  }
  lightService.send('START');
  return false;
}

window.inicioAInstancia = function() {
  const checkedValue = document.querySelector('input[name="inicio-a-instancia"]').checked;
  if (checkedValue == true) {
    lightService.send('INICIOAINSTANCIACHECKED');
  } else {
    lightService.send('INICIOAINSTANCIAUNCHECKED');
  }
  lightService.send('START');
  return false;
}

window.plazosRadios = function() {
  const checkedValue = document.querySelector('fieldset[id="plazos-radios-fieldset"] input:checked').value;
  switch (checkedValue) {
    case 'fueraDePlazo':
      lightService.send('FUERADEPLAZO');
      break;
    case 'enPlazo':
      lightService.send('ENPLAZO');
      break;
    case 'plazoProximo':
      lightService.send('PLAZOPROXIMO');
      break;
    case 'plazoPendiente':
      lightService.send('PLAZOPENDIENTE');
      document.getElementById('temporal-opciones-4').checked = true;
      break;
    default:
      lightService.send('FUERADEPLAZO');
  }
  lightService.send('START');
  return false;
}

window.formaPresencial = function() {
  const checkedValue = document.querySelector('input[name="forma-presencial"]').checked;
  if (checkedValue == true) {
    lightService.send('FORMAPRESENCIALCHECKED');
  } else {
    lightService.send('FORMAPRESENCIALUNCHECKED');
  }
  lightService.send('START');
  return false;
}
window.oficinasDeRegistro = function(event) {
  const checkedValue = document.querySelector('input[id="oficinas-de-registro"]').checked;
  if (checkedValue == true) {
    lightService.send('OFICINASDEREGISTROCHECKED');
  } else {
    lightService.send('OFICINASDEREGISTROUNCHECKED');
  }
  lightService.send('START');
  return false;
}
window.otroSitio = function(event) {
  const checkedValue = document.querySelector('input[id="otro-sitio"]').checked;
  if (checkedValue == true) {
    lightService.send('OTROSITIOCHECKED');
  } else {
    lightService.send('OTROSITIOUNCHECKED');
  }
  lightService.send('START');
  return false;
}
window.formaElectronico = function() {
  const checkedValue = document.querySelector('input[name="forma-electronico"]').checked;
  if (checkedValue == true) {
    lightService.send('FORMAELECTRONICOCHECKED');
  } else {
    lightService.send('FORMAELECTRONICOUNCHECKED');
  }
  lightService.send('START');
  return false;
}
window.formaElectronicoRadios = function() {
  const checkedValue = document.querySelector('fieldset[id="forma-electronico-radios-fieldset"] input:checked').value;
  switch (checkedValue) {
    case 'forma-electronico-tto':
      lightService.send('TTO');
      break;
    case 'forma-electronico-rega':
      lightService.send('REGA');
      break;
    case 'forma-electronico-plataforma-propia':
      lightService.send('PROPIA');
      break;
    default:
      lightService.send('TTO');
  }
  lightService.send('START');
  return false;
}
window.modeloRadios = function() {
  const checkedValue = document.querySelector('fieldset[id="modelo-radios-fieldset"] input:checked').value;
  switch (checkedValue) {
    case 'modelo-no':
      lightService.send('NONECESARIO');
      break;
    case 'modelo-si':
      lightService.send('SINECESARIO');
      break;
    default:
      lightService.send('SINECESARIO');
  }
  lightService.send('START');
  return false;
}
window.fechasRadios = function() {
  const checkedValue = document.querySelector('fieldset[id="fechas-radios-fieldset"] input:checked').value;
  switch (checkedValue) {
    case 'plazo-temporal':
      lightService.send('FECHASTEMPORAL');
      break;
    case 'plazo-permanente':
      lightService.send('FECHASPERMANENTE');
      break;
    default:
      lightService.send('FECHASPERMANENTE');
  }
  lightService.send('START');
  return false;
}

window.temporalRadios = function() {
  const checkedValue = document.querySelector('fieldset[id="temporal-radios-fieldset"] input:checked').value;
  switch (checkedValue) {
    case 'temporalInicioFin':
      lightService.send('TEMPORALINICIOFIN');
      break;
    case 'temporalInicio':
      lightService.send('TEMPORALINICIO');
      break;
    case 'temporalFin':
      lightService.send('TEMPORALFIN');
      break;
    case 'temporalVacios':
      lightService.send('TEMPORALVACIOS');
      break;
    default:
      lightService.send('TEMPORALINICIOFIN');
  }
  lightService.send('START');
  return false;
}

window.formaElectronicoOpcionesAdicionales = function(event) {
  const checkedValue = document.querySelector('input[name="forma-electronico-opciones-adicionales"]').checked;
  if (checkedValue == true) {
    lightService.send('FORMAELECTRONICOOPCIONESADICIONALESCHECKED');
  } else {
    lightService.send('FORMAELECTRONICOOPCIONESADICIONALESUNCHECKED');
  }
  return false;
}

window.formaElectronicoPersonalFuncionario = function(event) {
  const checkedValue = document.querySelector('input[name="forma-electronico-designar-personal-funcionario-habilitado"]').checked;
  if (checkedValue == true) {
    lightService.send('FORMAELECTRONICOPERSONALFUNCIONARIOHABILITADOCHECKED');
  } else {
    lightService.send('FORMAELECTRONICOPERSONALFUNCIONARIOHABILITADOUNCHECKED');
  }
  return false;
}


window.otrasCondicionesEspecificasDePresentacionDeLaSolicitud = function(event) {
  const checkedValue = document.querySelector('input[name="otras-condiciones-especificas-de-presentacion-de-la-solicitud"]').checked;
  if (checkedValue == true) {
    lightService.send('OTRASCONDICIONESESPECIFICASDEPRESENTACIONDELASOLICITUDCHECKED');
  } else {
    lightService.send('OTRASCONDICIONESESPECIFICASDEPRESENTACIONDELASOLICITUDUNCHECKED');
  }
  return false;
}


window.onload = function(){
  // Start the service
  lightService.start();
  // Look at the dependency: https://xstate.js.org/
  inicioDeOficio();
  inicioAInstancia();
  plazosRadios();
  formaPresencial();
  oficinasDeRegistro();
  otroSitio();
  formaElectronico();
  formaElectronicoRadios();
  modeloRadios();
  fechasRadios();
  temporalRadios();
  formaElectronicoOpcionesAdicionales();
  formaElectronicoPersonalFuncionario();
  otrasCondicionesEspecificasDePresentacionDeLaSolicitud();
}


