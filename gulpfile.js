const gulp = require('gulp');
const { series, parallel } = require('gulp');
const clean = require('gulp-clean');
const nunjucksRender = require('gulp-nunjucks-render');
const browserSync = require('browser-sync').create();
const postcss = require('gulp-postcss');
const atimport = require('postcss-import');
const tailwindnesting = require('tailwindcss/nesting');
const stripCssComments = require('gulp-strip-css-comments');
const footer = require('gulp-footer');
const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const imagemin = require('gulp-imagemin');

/* This config file adds this project's files to be purged with PurgeCSS */
const TAILWIND_CONFIG = './config/tailwind.config.js';
/* This project's files to be compiled */
const SOURCE_HTML_DIR = './src/**.html';
/* This project's files AND desy files to be searched for in nunjucks recursive compilation */
const SOURCE_NUNJUCKS_PATHS = ['./src/templates/','./node_modules/desy-html/src/templates/'];
/* This project's files to be watched */
const SOURCE_NUNJUCKS_FILES = ['./src/templates/**/*'];
/* This project's html files to be compiled */
const SOURCE_NUNJUCKS_DIR = ['./src/**/*.html'];

const SOURCE_JS_FILES = ['./node_modules/desy-html/src/js/**/*.js','!./node_modules/desy-html/src/js/globals/**','!./node_modules/desy-html/src/js/filters/**','./node_modules/xstate/dist/*.js', './src/js/**/*.js'];
const SOURCE_STYLESHEET = './src/css/styles.css';
const SOURCE_STYLESHEET_DIR = './src/**/*.css';
const SOURCE_IMG_DIR = './src/img/**.*';
const DESTINATION_HTML_DIR = './dist/';
const DESTINATION_JS_DIR = './dist/js';
const DESTINATION_IMG_DIR = './dist/images';
const DESTINATION_STYLESHEET = './dist/css/';


function bs(cb) {
  browserSync.init({
    server: {
      baseDir: './dist/'
    }
  });

  cb();
}


function watchFiles(cb) {
  gulp.watch([TAILWIND_CONFIG, SOURCE_STYLESHEET_DIR, SOURCE_HTML_DIR, ...SOURCE_JS_FILES, ...SOURCE_NUNJUCKS_DIR, ...SOURCE_NUNJUCKS_FILES], gulp.series(html, nunjucks, js, css, reload));
  cb();
}


function reload(cb) {
  browserSync.reload();
  cb();
}


function css() {
  return gulp.src(SOURCE_STYLESHEET)
    .pipe(
      postcss([
      atimport(),
      tailwindnesting(),
      tailwindcss(TAILWIND_CONFIG),
      autoprefixer()
      ])
    )
    .pipe(stripCssComments({preserve: false}))
    .pipe(footer('\n'))
    .pipe(gulp.dest(DESTINATION_STYLESHEET));
}


function html() {
  return gulp.src(SOURCE_HTML_DIR)
    .pipe(gulp.dest(DESTINATION_HTML_DIR));
}


function img() {
  return gulp.src(SOURCE_IMG_DIR)
    .pipe(imagemin())
    .pipe(gulp.dest(DESTINATION_IMG_DIR));
}


function imgDev() {
  return gulp.src(SOURCE_IMG_DIR)
    .pipe(gulp.dest(DESTINATION_IMG_DIR));
}


function nunjucks() {
  return gulp.src(SOURCE_NUNJUCKS_DIR)
    .pipe(nunjucksRender({
        envOptions: {
          autoescape: true,
          trimBlocks: true,
          lstripBlocks: true,
          noCache: true
        },
        path: SOURCE_NUNJUCKS_PATHS // String or Array
      }))
    .pipe(gulp.dest(DESTINATION_HTML_DIR));
}


function js() {
  return gulp.src(SOURCE_JS_FILES)
    .pipe(gulp.dest(DESTINATION_JS_DIR));
}


function version() {
  return gulp.src('./package.json')
    .pipe(gulp.dest('./dist/'));
}


function license() {
  return gulp.src('./src/EUPL-1.2.txt')
    .pipe(gulp.dest('./dist/'));
}


function cleanFolder() {
  return gulp.src(DESTINATION_HTML_DIR, {read: false, allowEmpty: true})
    .pipe(clean());
}


exports.default = series(cleanFolder, html, imgDev, nunjucks, js, css, license, version, bs, watchFiles);
exports.prod = series(cleanFolder, html, img, nunjucks, js, css, license, version, bs);