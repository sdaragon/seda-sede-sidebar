# Welcome to seda-sede-sidebar 👋
![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)
![Prerequisite](https://img.shields.io/badge/npm-%3E%3D6.14.0-blue.svg)
![Prerequisite](https://img.shields.io/badge/node-%3E%3D12.18.0-blue.svg)
[![License: EUPL--1.2](https://img.shields.io/badge/License-EUPL--1.2-yellow.svg)](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)

> Este es un proyecto para visualizar la relación entre los datos que se introducen en SEDA y cómo se visualizan en SEDE

### 🏠 [Homepage](https://desy-ast.aragon.es/)

## Prerequisites

- npm >=6.14.0
- node >=12.18.0

## Install

```sh
npm install
```

## Author

👤 **Desy (SDA Servicios Digitales de Aragón)**


## Show your support

Give a ⭐️ if this project helped you!


## 📝 License

This project is [EUPL--1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_